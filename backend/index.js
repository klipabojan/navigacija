const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const sqlite3 = require("sqlite3").verbose();
const dbConfig = require("./db.config.js");
const jwt = require("jsonwebtoken");

const mysql = require("mysql");
const cors = require("cors");
app.use(
  cors({
    origin: "*",
  })
);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

let db = new sqlite3.Database("./db.db", sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log("Connected to db.");
});

const dbConn = mysql.createConnection({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB,
});

dbConn.connect();

app.get("/cijenagoriva", function (request, response) {
  dbConn.query(
    "SELECT * FROM cijenagoriva order by id_cijene desc limit 2",
    function (error, results, fields) {
      if (error) throw error;
      return response.send({
        error: false,
        data: results,
        message: "cijenagoriva_list.",
      });
    }
  );
});

app.post("/cijenagoriva", (request, response) => {
  const token = request.headers["authorization"];
  if (token !== "null") {
    jwt.verify(token, "111111111", function (err, decoded) {
      if (err) {
        response.send("error");
      }
    });

    const cijene = request.body;
    dbConn.query(
      "INSERT INTO cijenagoriva (benzin,dizel,plin) VALUES (?,?,?)",
      [cijene.benzin, cijene.dizel, cijene.plin],
      function (error, results, fields) {
        if (error) throw error;
        return response.send({
          error: false,
          data: results,
          message: "cijenagoriva_list.",
        });
      }
    );
  } else {
    response.send("Prijavi se");
  }
});

app.post("/prijava", function (request, response) {
  const korisnik = request.body;
  if (korisnik.email !== null && korisnik.sifta !== null) {
    dbConn.query(
      "SELECT * FROM admin where email=?",
      [korisnik.email],
      function (error, results, fields) {
        if (error) throw error;
        if (results.length > 0) {
          let admin = results[0];
          if (admin.sifra === korisnik.sifra) {
            let token = jwt.sign({ id: admin.id_admin }, "111111111");
            response.send({ token });
          } else {
            response.send("Pogrešan email ili lozinka!");
          }
        } else {
          response.send("Pogrešan email ili lozinka!");
        }
      }
    );
  } else response.send("Unesite email i lozinku");
});

app.get("/zadnjaPotrosnja", (req, res) => {
  const sqlSelect = "select * from potrosnja order by id desc limit 1";
  db.all(sqlSelect, (err, result) => {
    res.send(result);
  });
});

app.post("/spremiPotrosnju", (request, response) => {
  const data = request.body;
  db.all(
    "INSERT INTO potrosnja (prosjecnaPotrosnja,kilometri,trosak) VALUES (?,?,?)",
    [data.prosjecnaPotrosnja, data.kilometri, data.trosak],
    function (error, results, fields) {
      if (error) throw error;
      return response.send(results);
    }
  );
});

app.listen(3000, () => {
  console.log("port 3000");
});
