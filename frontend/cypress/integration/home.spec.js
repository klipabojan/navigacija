/// <reference types="cypress" />
// Use `cy.dataCy` custom command for more robust tests
// See https://docs.cypress.io/guides/references/best-practices.html#Selecting-Elements

// ** This file is an example of how to write Cypress tests, you can safely delete it **

// This test will pass when run against a clean Quasar project
describe("Potrosnja", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("izracun potrosnje", () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get(
      '[href="/potrosnja"] > .q-img > .q-img__container > .q-img__image'
    ).click();
    cy.dataCy("maxtest").type("60");
    cy.dataCy("prijedeni").type("628");
    const lijevo = "{leftArrow}".repeat(34);
    const desno = "{rightArrow}".repeat(50);
    cy.dataCy("prijeputa").type(desno);
    cy.dataCy("poslijeputa").type(lijevo);
    cy.dataCy("izracun").should("have.text", "8.03/100km ");
    /* ==== End Cypress Studio ==== */
  });
  it("krivi izracun potrosnje", () => {
    /* ==== Generated with Cypress Studio ==== */
    cy.get(
      '[href="/potrosnja"] > .q-img > .q-img__container > .q-img__image'
    ).click();
    cy.dataCy("maxtest").type("60");
    cy.dataCy("prijedeni").type("628");
    const lijevo = "{leftArrow}".repeat(40);
    const desno = "{rightArrow}".repeat(50);
    cy.dataCy("prijeputa").type(desno);
    cy.dataCy("poslijeputa").type(lijevo);
    cy.dataCy("izracun").should("not.have.text", "8.03/100km ");
    /* ==== End Cypress Studio ==== */
  });
});

// ** The following code is an example to show you how to write some tests for your home page **
//
// describe('Home page tests', () => {
//   beforeEach(() => {
//     cy.visit('/');
//   });
//   it('has pretty background', () => {
//     cy.dataCy('landing-wrapper')
//       .should('have.css', 'background').and('match', /(".+(\/img\/background).+\.png)/);
//   });
//   it('has pretty logo', () => {
//     cy.dataCy('landing-wrapper img')
//       .should('have.class', 'logo-main')
//       .and('have.attr', 'src')
//       .and('match', /^(data:image\/svg\+xml).+/);
//   });
//   it('has very important information', () => {
//     cy.dataCy('instruction-wrapper')
//       .should('contain', 'SETUP INSTRUCTIONS')
//       .and('contain', 'Configure Authentication')
//       .and('contain', 'Database Configuration and CRUD operations')
//       .and('contain', 'Continuous Integration & Continuous Deployment CI/CD');
//   });
// });
