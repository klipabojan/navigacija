const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/PocetnaStranica.vue") },
      {
        path: "potrosnja",
        component: () => import("pages/PotrosnjaGoriva.vue"),
      },
      { path: "prijava", component: () => import("pages/PrijavaAdmina.vue") },
      {
        path: "cijenegoriva",
        component: () => import("pages/CijeneGoriva.vue"),
      },
      {
        path: "navigacija",
        component: () => import("pages/NavigacijaStr.vue"),
      },
      {
        path: "adminpromjene",
        component: () => import("pages/AdminPromjene.vue"),
        meta: { requiresAuth: true },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
